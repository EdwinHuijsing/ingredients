# Translate Ingredients
This project has been started in around 2007-10, but got on-hold in 2008-08 (its still on my TODO list).

While learning PHP this is the first site that uses a MySql back-end and is  
multilingual. Also css is more used, but the focus still is PHP.  
Comments are written in Dutch. Some functions will not work, that's why they  
are in comment. Or the functions are not complete or just to test some thing i  
would like to have.

In this project the advantage of templates became more clear, also the lack of  
database knowledge came to the front. Some parts of the database, looked OK to  
me, but it became a problem to write the sql statements to show the data.  
At this point i started to go to school to lean it the right way, this is also  
the reason i never finished this project.  
While looking back at the code, I see a lot of old habits and lack of  
knowledge. I want to update it, but for now i have a other project that i need.


While installing this project in 2016, i found one real problem: there are no  
backup or sql dump file(s). The only thing i had are the database files.  
Almost 95% of the database got imported OK, only to tables did not restore.  
The table names and the columns where there, so i recreated them with the  
information i had. after some test runs all is working again.  

**Note:**  
The data in the sql file should be correct, but i served as a start point. The  
data is not complete, and there are many languages missing.  
