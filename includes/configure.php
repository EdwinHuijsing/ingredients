<?php
/*

  Id: configure.php,v 1.0 2009/03/07 16:29

*/

// Define the webserver and path parameters
// * DIR_FS_* = Filesystem directories (local/physical)
// * DIR_WS_* = Webserver directories (virtual/URL)
  define('HTTP_SERVER', 'http://localhost'); // eg, http://localhost - should not be empty for productive servers
  define('DIR_WS_HTTP_ROOT', HTTP_SERVER . '/');
  define('DIR_WS_IMAGES', 'images/');
  define('DIR_WS_BANNERS', DIR_WS_IMAGES . 'banners/');
  define('DIR_WS_INCLUDES', 'includes/');
  define('DIR_WS_LOG', 'log/');
  define('DIR_WS_NEW', 'new/');
  define('DIR_WS_TRANS', 'trans/');
  define('DIR_WS_BOXES', DIR_WS_INCLUDES . 'boxes/');
  define('DIR_WS_FUNCTIONS', DIR_WS_INCLUDES . 'functions/');
  define('DIR_WS_CLASSES', DIR_WS_INCLUDES . 'classes/');
   define('DIR_WS_LANGUAGES', DIR_WS_INCLUDES . 'languages/');

// define our database connection
  define('DB_SERVER', 'localhost'); // eg, localhost - should not be empty for productive servers
  define('DB_SERVER_USERNAME', 'root');
  define('DB_SERVER_PASSWORD', '');
  define('DB_DATABASE', 'ingredients');
?>
