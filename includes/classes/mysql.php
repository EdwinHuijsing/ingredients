<?php
/*

  Id: mysql.php,v 1.0 2009/03/07 16:29

*/
class qmysql{
    //de variabelen
    var $connect;
    var $link;
    var $LOG;

    //de connect functie
    function qconnect($server = DB_SERVER, $username = DB_SERVER_USERNAME, $password = DB_SERVER_PASSWORD, $database = DB_DATABAS){
        $this->LOG = DIR_WS_LOG;
        global $cfg;
        global $_SERVER;
        $this->connect = @mysql_connect($host, $user, $pass);
        if(empty($this->connect)){
            echo "er is een fout opgetreden bij het verbinden met mysql:<br>\n";
            echo mysql_error() . "<br>\n";
            echo "foutnummer: ". mysql_errno();
            $error = $this->date_log() . " error(mysql_connect): ". mysql_error() . " errno: " . mysql_errno() . " in page: " . $_SERVER['SCRIPT_NAME'] ."?" . $_SERVER['QUERY_STRING'] ." on ip: ". $_SERVER['REMOTE_ADDR'] . "\n";
            $this->LOG .= $error;
            $this->close();
            exit();
        }
        $this->link = @mysql_select_db($db, $this->connect);
        if(empty($this->link)){
            echo "er is een fout opgetreden bij het verbinden met mysql:<br>\n";
            echo mysql_error() . "<br>\n";
            echo "foutnummer: " . mysql_errno();
            $error = $this->date_log() . " error(mysql_select_db): " . mysql_error() . " errno: " . mysql_errno() . " in page: " . $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING'] . " on ip: ". $_SERVER['REMOTE_ADDR'] . "\n";
            $this->LOG .= $error;
            $this->close();
            exit();
        }
        return $this->link;
    }

    //de functies
    //eerst de constructor
    function mysql($host, $user, $pass, $db){
        $this->connect($host, $user, $pass, $db);
    }

    //de __query functie
    function __query($sql){
        $result = mysql_query($sql, $this->connect);
        global $_SERVER;
        global $cfg;
        if(empty($result)){
            echo "er is een fout opgetreden bij het verzenden van de query:<br>\n";
            echo mysql_error() . "<br />\n";
            echo "foutnummer: ". mysql_errno();
            $error = $this->date_log() . " error(mysql_query): " . mysql_error() ." errno: ". mysql_errno() ." in page: ".$_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING'] ." on ip: ". $_SERVER['REMOTE_ADDR'] . "\n";
            $this->LOG .= $error;
            $this->close();
            exit();
        }
        return $result;
    }

    //de query functie
    function query($var){
        if(is_array($var)){
            foreach($var AS $key => $sql){
                $result[$key] = $this->__query($sql);
            }
        }else{
            $result = $this->__query($var);
        }
        return $result;
    }

    //de array functie
    function fetch_array($result){
        $array = mysql_fetch_array($result);
        return $array;
    }

    //de row functie
    function fetch_row($result){
        $array = mysql_fetch_assoc($result);
        return $array;
    }

    //de assoc functie
    function fetch_assoc($result){
        $array = mysql_fetch_assoc($result);
        return $array;
    }

    //de object functie
    function fetch_object($result){
        $obj = mysql_fetch_object($result);
        return $obj;
    }

    //de date_log functie
    function date_log(){
        return date("d-m-Y H:i:s :");
    }

    //de close functie
    function close(){
        $fp = fopen("log_mysql.txt", "a");
        fwrite($fp, $this->LOG);
        fclose($fp);
        mysql_close($this->connect);
    }
}
