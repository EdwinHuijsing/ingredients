<?php
/*

  Id: header_app.php,v 1.0 2009/03/07 16:29

*/

// Include filenames often required
require DIR_WS_INCLUDES . 'files.php';
require DIR_WS_INCLUDES . 'db_table.php';
require DIR_WS_FUNCTIONS . 'language.php';

// include functions
  require(DIR_WS_FUNCTIONS . 'database1.php');
  require(DIR_WS_FUNCTIONS . 'main.php');
  require(DIR_WS_FUNCTIONS . HTML_OUTPUT);
  Include(DIR_WS_FUNCTIONS . 'banner.php');

// make a connection to the database
  fundb_connect();

// start session
    session_start();
        define('PHP_SESSION_NAME', 'lightsid');

// Get browser language
require (DIR_WS_INCLUDES . 'selectlanguage.php');
Include DIR_WS_LANGUAGES . $_SESSION['language'] . ".php";

// perment strings
$ses_lng_desc = htmlspecialchars($_SESSION['language'],ENT_QUOTES,"UTF-8");

// error modus ivm weergave van fout meldingen
// 0 is offline/local : 1 = online/publice
define ('ONLINE_LIVE', '0');

?>
