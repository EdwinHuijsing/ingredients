<?php
/*
  $Id: database.php,v 1.21 2003/06/09 21:21:59 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

<?php
function f_db_connect(){
  global $$link;
  mysql_connect(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD) or die('Unable to connect to database server!');
  mysql_select_db(DB_DATABASE) or die(mysql_error());

  return $$link;
}


function f_db_close($$link = 'db_link'){
  global $$link;
  return mysql_close($$link);
}

function f_db_query($query){
  global $$link;
  $return = mysql_query($query) or die(error1);
  return $return;
}
?>
