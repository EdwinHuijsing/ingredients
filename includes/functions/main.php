<?php
/*

  Id: main.php,v 1.0 2007/03/18 21:49
  thx to http://www.php-mysql-tutorial.com/ for the code where it start with  // bof function prenex = previous next page

*/

function prenex($aquery, $rowsPerPage, $offset){
// function prenex($aquery, $rowsPerPage, $offset){
/*    // how many rows to show per page
    $rowsPerPage = 10;

    // by default we show first page
    $pageNum = 1;
*/
    // if $_GET['page'] defined, use it as page number
    if(isset($_GET['page']))
    {
        $pageNum = $_GET['page'];
    }else{
        $pageNum = 1;
    }


    // how many rows we have in database
    $result  = mysql_query($aquery) or die('Error, query failed1');
    $row     = mysql_numrows($result);
    $numrows = $row;


    // how many pages we have when using paging?
    $maxPage = ceil($numrows/$rowsPerPage);

    // print the link to access each page
    $self = $_SERVER['PHP_SELF'];
    $nav = '';
    for($page = 1; $page <= $maxPage; $page++)
    {
        if ($page == $pageNum)
        {
            $nav .= '<a class="prenex">[' . $page . ']</a>';   // no need to create a link to current page
        }
        else
        {
            $nav .= " <a href=\"$self?page=$page\">$page</a> ";
        }
    }

    // creating previous and next link
    // plus the link to go straight to
    // the first and last page

    if ($pageNum > 1)
    {
        $page = $pageNum - 1;
        $prev = " <a href=\"$self?page=$page\">[Prev]</a> ";

        $first = " <a href=\"$self?page=1\">[First Page]</a> ";
    }
    else
    {
        $prev  = '<font class="prenex">[Prev]</font>'; // we're on page one, don't print previous link
        $first = '<font class="prenex">[First Page]</font>'; // nor the first page link
    }

    if ($pageNum < $maxPage)
    {
        $page = $pageNum + 1;
        $next = " <a href=\"$self?page=$page\">[Next]</a> ";

        $last = " <a href=\"$self?page=$maxPage\">[Last Page]</a> ";
    }
    else
    {
        $next = '<font class="prenex">[Next]</font>'; // we're on the last page, don't print next link
        $last = '<font class="prenex">[Last Page]</font>'; // nor the last page link
    }

    // print the navigation link
    echo $first . $prev . $nav . $next . $last;
}
// bof function prenex = previous next page
?>
