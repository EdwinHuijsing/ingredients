<?php
/*

  Id: language.php,v 1.0 2009/03/07 16:29

*/

/*
   hier wordt gekeken wat de standaart taal van de browers is.
   als er niet al een taal is 
*/


//
$_SESSION['lang_id'] = 'nl';

function fun_change_lang($lng){
    /*
    explode splits de deel waar gevraagd (bv waar een ? staat) door alleen het achterste gedeelte
    te gebruiken word het deel steeds kleiner en kan ik alleen dat gebruiken wat ik wil hebben.
    Als alles de nieuwe waarde heeft moet dit opnieuw zamen gezet worden voordat het weer zamen 
    gevoerd kan worden(implode).
    */
    $url = $_SERVER['REQUEST_URI'];
    $url1 = explode('?', $url);
//     print_r($url1);
    if (!empty($url1[1]) ) {
      $url2 = explode('&', $url1[1]);
      if (isset($_GET['lng'])){
         $i=0;
         while ($i < count($url2)){
         $a = $url2[$i];
         $a1 = explode('=', $a);
         $ii=0;
         while ($ii < count($a1)){
            if ($a1[0] == 'lng'){
               $a1[1] = $lng;
            }
            $ii++;
            break;
         }
         $url2[$i] = implode('=', $a1);
         $i++;
         }
      }else{
         $i=0;
         while ($i < count($url2)){
            $i++;
         }
         $url2[$i] = 'lng=' . $lng;
      }
      $url2 = implode('&', $url2);//samenvoegen nieuwe array
      $url1 = array($url1[0], $url2);
      $url1 = implode('?', $url1);
      $base_url = (HTTP_SERVER);
      $url_compl = array($base_url, $url1);
      $url = htmlspecialchars(implode('', $url_compl));
      return $url;
    }else{
         $i=0;
         while ($i < count($url)){
            $i++;
         }
         $url2 = array('?', '&lng=' . $lng);
         $url1 = $url2[1];
         $url = htmlspecialchars(implode('', $url2));
         return $url;
    }
}

// function fun_change_lang1($lang){
//    while ($split_url= explode('&', $_SERVER['QUERY_STRING'])) {
//       if echo $split_url
//    }
   
//    $langcomp = 'lang=' . $lang;// maak de string voor de url bv 'lang=nl'
//    $urli = array($_SERVER['QUERY_STRING'], $langcomp);// zet de variablelen op de goede plaats voor het zamen voegen
//    $urlt = implode('&', $urli);// is het gedeelte na de ? zamen gesteld
//    $base = ($_SERVER['PHP_SELF']);// verzamel de gegevens voor de ?
//    $a_complete = array($base, $urlt);// stel de complete url zamen
//    $complete = implode('?', $a_complete);// de url is nu zamen gesteld
//    return $complete;// dit zorgt er voor dat de waarde word terug geven aan de plaats waar het vandaan komt
// }

?>