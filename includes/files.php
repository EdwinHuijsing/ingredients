<?php
/*

  Id: files.php,v 1.0 2009/03/07 16:29

*/

// Box
define ('LANGUAGE', DIR_WS_BOXES . 'language.php');

//Functions
define ('LANGCHOICE', 'langchioce.php');
define ('HTML_OUTPUT', 'html_output.php');

// FRAME
Define ('FILENAME_COL_LEFT', 'column_left.php');
Define ('FILENAME_COL_RIGHT', 'column_right.php');
Define ('FILENAME_FOOTER', 'footer.php');


// NEW
Define ('FILENAME_NEW_CAT', 'new_cat.php');
Define ('FILENAME_NEW_INGR', 'newingr.php');
Define ('FILENAME_NEW_INGR_SUC', 'newingr_succes.php');
Define ('FILENAME_NEW_CAT_SUC', 'new_cat_succes.php');
Define ('FILENAME_NEW_LANG', 'newlang.php');
Define ('FILENAME_NEW_LANG_SUC', 'newlang_succes.php');

// Define ('', '');

//Edit
Define ('FILENAME_EDIT_INGR', 'edit_ingr.php');

// OVERIGE
Define ('FILENAME_TRANSLATE', 'translate.php');
Define ('FILENAME_CAT', 'categorie.php');
Define ('FILENAME_TRANS_CAT', 'trans_cat.php');
Define ('FILENAME_REDIRECT', 'redirect.php');

?> 