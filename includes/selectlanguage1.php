<?php
/*

  Id: selectlanguage1.php,v 1.0 2009/03/07 16:29

*/

    include('functions/langchoice.php');
//Start Taalkeuze script
//session_start();
//$_SESSION['language'] = session_name('language');
//Kijkt of er een handmatige taalkeuze is gekozen, en kijkt dan in de sessie.
if (isset($_SESSION['language'])) {
    langchoice($_SESSION['language']);
    //Kijkt of de handmatige taalkeuze word veranderd, en set de sessie weer opnieuw.
    if (isset($_GET) && isset($_GET['lang'])) {
        $_SESSION['language'] = $_GET['lang'];
        langchoice($_SESSION['language']);
  }
}
// Als geen sessie is geset, hier verder.
else {
    //Als er handmatige keuze word gemaakt, word de sessie geset.
    if (isset($_GET) && isset($_GET['lang'])) {
        $_SESSION['language'] = $_GET['lang'];
        langchoice($_SESSION['language']);
    }
    // Controleer of er reeds een cookie is gezet
    else {
        if (isset($_COOKIE['language'])){
        $_SESSION['language'] = $_COOKIE['language'];
        }
        //Als er geen sessie is geset, en er geen handmatige taalkeuze is gekozen, dan pakt hij de taal van de browser.
        else {
            $browserlang = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2); // de funtie substr verwijdert delen van de string waardoor hier nu nog maar een taal overblijft
            defaultBowserLang($browserlang);
            //$_SESSION["language"] = $browserlang;
        }
    }
}
// De cookie elke keer na 28 dagen vernieuwen
setcookie("language", $_SESSION["language"] ,time()+(28*24*60*60));
?>
