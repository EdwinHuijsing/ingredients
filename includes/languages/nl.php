<?php
/*

  Id: nl.php,v 1.0 2009/03/07 16:29

*/

Define ('TITLE', '2Be it :: tijdens het koken, Ingredienten vertaal service');
Define ('DESCRIPTION', 'Ingredienten vertaal site');

//CENTER
Define ('WELCOME_MAIN', '<h1>&nbsp;&nbsp;Welkom bij 2Be it<h1><h2>&nbsp;&nbsp;De Ingredienten vertaal Service</h2><br><br><br>Dit is de eerst opzet van een online vertaal database voor ingrediënten.<br>Er moet nog veel aan gebeuren, maar het begin is gemaakt.<br>Klik op de categorie om de bij behoorden ingredienten te zien<br><br>Veel plezier met de ingrediënten.<br><br>');
//Boxes
Define ('TXT_HEADER', 'Welkom bij onze vertaalservice! 2Be it');
Define ('HOME', 'Begin');
Define ('ALL_ING', 'Alle Ingredienten');
Define ('ING_CAT', 'Per categorie');
Define ('TRANS_ING', 'Om het ingredient te vertalen moet u op de naam van het ingredient klikken.');
Define ('NEW_LANG', 'Taal toevoegen');

// New
Define ('NAME_NEW_LANG', 'Naam taal');
Define ('NAME_NEW_LANG_CODE', 'Taal code');
Define ('NAME_NEW_CAT', 'Categorie toevoegen');
// TEMP
Define('DB_TEMP_OFF', 'Invoeren in de DB is tijdelijk uitgezet!! (hard code)');
Define('NAME_CAT', 'Categorie vertalen');
?>