<?php
/*

  Id: db_table.php,v 1.0 2007/11/04 03:36

*/
// tables
define ('TABLE_CAT', 'categorie');
define ('TABLE_CAT_DESC', 'categorie_description');
define ('TABLE_ING', 'ingredients');
define ('TABLE_ING_DESC', 'ingredients_description');
define ('TABLE_LNG', 'language');
define ('TABLE_LNG_DESC', 'language_description');


/********************************/
// Fiels
/********************************/
//
//
//
define ('FLD_ING_ID', 'ing_id');
define ('FLD_LANG_ID', 'lang_id');


//
// TBL_ING_DESC
//
define ('FLD_ING_DESC', 'ing_Desc');
define ('FLD_ING_TRANSDESC', 'ing_TransDesc');

//
// TBL language_description
//
define ('FLD_LANG_DESC', 'lang_desc');
?>
