<?php
/*

  Id: selectlanguage.php,v 1.0 2009/03/07 16:29

*/

include('functions/langchoice.php');
//Start Taalkeuze script

//Kijkt of er een handmatige taalkeuze is gekozen, en kijkt dan in de sessie.
if (isset($_SESSION['language'])) {
    langchoice($_SESSION['language']);

    //Kijkt of de handmatige taalkeuze word veranderd, en set de sessie weer opnieuw.
    if (isset($_GET['lng'])) {
        $_SESSION['language'] = $_GET['lng'];
        langchoice($_SESSION['language']);
    }
// Als geen sessie is geset, hier verder.
} else {
    //Als er handmatige keuze word gemaakt, word de sessie geset.
    if (isset($_GET) && isset($_GET['lng'])) {
        $_SESSION['language'] = $_GET['lng'];
        langchoice($_SESSION['language']);
    //Als er geen sessie is geset, en er geen handmatige taalkeuze is gekozen, dan pakt hij de taal van de browser.
    } else {
        $browserlang = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2); // de funtie substr verwijdert delen van de string waardoor hier nu nog maar een taal overblijft
        defaultBowserLang($browserlang);
    }
}
?>
