<?php
/*

  Id: index.php,v 1.0 2007/03/01 01:43

*/
$_SERVER['REQUEST_URI'];
// error_reporting (E_ALL);
?>
<!-- bof HEADER -->
    <?php require "includes/head.php";?>
<!-- eof HEADER -->
<!-- bof BODY -->
<!-- bof Check for errors -->
    <?php  require DIR_WS_INCLUDES . "error.php"; ?>
<!-- eof Check for errors -->
<table class="Main">
    <tr>
<!-- bof colum left -->
    <td class="column">
    <?php require DIR_WS_INCLUDES . FILENAME_COL_LEFT; ?>
    </td>
<!-- eof colum left -->
<!-- bof center -->
        <td class="center">
        <?php //require "temp.php";
        // const
        define ('ERROR_NAAM', 'Er zit een fout in de naam!!');
        //$naam = strip_tags($_POST['naam']); // deze verwijdert alle html en php tags
        //$naam = htmlspecialchars($_POST['naam']); // zet de charters om naar ascii

        // lees post variable in
        $email_naam  = htmlspecialchars($_POST['naam']);
        $email_email = htmlspecialchars($_POST['email']);
        $email_title = strip_tags($_POST['title']);
        $email_vraag = $_POST['vraag'];

        // echo $naam;
        $checkvraag = "/^[[:alnum:][:space:]\/$<>:;.,!@#%&*()+_-s]*$/"; // [:alpha:] == [a-zA-z]
        $pregtekst = "/^([[:alpha:][:digit:][:space:]_-]{3,})$/";
//          $pregMail = "/^([[:alpha:]]+([[:alpha:][:digit:]_-])+(\.[[:alpha:][:digit:]_-]*))+\@(([[:alpha:][:digit:]_-]*)+(\.[[:alpha:][:digit:]_-]{2,3}))$/";
        $pregMail = "/^(([[:alpha:]]+([[:alpha:][:digit:]_-])+(\.[[:alpha:][:digit:]_-]*))|([[:alpha:]]+([[:alpha:][:digit:]_-]*)))+\@((([[:alpha:][:digit:]_-]*)+(\.[[:alpha:][:digit:]_-]{2,3}))|(([[:alpha:][:digit:]_-]*)+(\.[[:alpha:][:digit:]_-]*)+(\.[[:alpha:][:digit:]_-]{2,3})))/";
        if (isset($_POST['submit']) ) {
            if(preg_match($pregtekst, $email_naam) ) {
                echo $email_naam;
                if(preg_match($pregMail, $email_email) ) {
                    echo "<br>";
                    echo $email_email;
                    if(preg_match($checkvraag, $email_vraag) ) {
                        if(preg_match($pregtekst, $email_title)) {
                            $mime_boundary = "----MSA Shipping----".md5(time());
                            $to = 'e.huijsing@2be-it.com';
                            $subject  = "Webvraag : " . $email_title;
                            $message  = "Van<br>\r\n";
                            $message .= "&nbsp;&nbsp;&nbsp;";
                            $message .= "Naam&nbsp;: ". $email_naam . " ;<br>\r\n";
                            $message .= "&nbsp;&nbsp;&nbsp;";
                            $message .= "E-mail: ". $email_email . " ;<br>\r\n";
                            $message .= "&nbsp;&nbsp;&nbsp;";
                            $message .= "Title&nbsp;&nbsp;&nbsp;: " . $email_title . " ;<br>\r\n";
                            $message .= $email_vraag;
                            $headers  = "Return-Receipt-To:\r\n";
                            $headers .= "MIME-Version: 1.0\r\n";
                            $headers .= "Content-type: multipart/alternative;; charset=iso-8859-1\r\n";
                            $headers .= "From: 2be-it Contact Us <NoReply@2be-it.com>\r\n";
                            $cc       = "";
                            $bcc      = "e.huijsing@2be-it.com";

                            if(mail( $to, $subject, $message, $headers)) {
//                            if(imap_mail( $to, $subject, $message, $headers, $cc, $bcc)) {
//                              mail( 'e.huijsing@2be-it.com', $subject, $message, $headers)
                               echo "Mail send";
                            }else{
                               echo "Error";
                            }
                        }else{
                            echo "Error title";
                        }
                    }else{
                    echo "Error vraag";
//                     echo "<br>";
//                     echo $vraag;
                    }
                }else{
                    echo "Error Mail";
                }
            }else{
               echo ERROR_NAAM;
            }


        } else {
         ?>
         <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
            <fieldset>
               <legend>Met dit formulier kunt u ons een vraag stellen.</legend>
               <table border = "0">
                    <tr>
                        <td><label class="formleft" for="naam">Uw naam</label></td>
                        <td><input type="text" name="naam" size="30"></td>
                    </tr>
                    <tr>
                        <td><label class="formleft" for="email">Uw e-mail-adres</label></td>
                        <td><input type="text" name="email"  size="30"></td>
                    </tr>
                    <tr>
                        <td><label class="formleft" for="title">Title</label></td>
                        <td><input type="text" name="title"  size="30"></td>
                    </tr>
                    <tr>
                        <td  valign="top"><label class="formleft" for="vraag">Uw vraag en /<br>of opmerking</label></td>
                        <td><textarea cols="39" rows="10" name="vraag"></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="submit" class="formsubmit" value="Vraag verzenden"></td>
                    </tr>
               </table>
            </fieldset>
         </form>
         <?php }
      ?>
    </td>
<!-- eof center -->
<!-- bof colum right -->
    <td class="column">
    <?php require DIR_WS_INCLUDES . FILENAME_COL_RIGHT; ?>
  </td>
<!-- eof colum right -->
    </tr>
</table>
<!-- eof BODY -->
<!-- bof FOOTER -->
    <?php //require DIR_WS_INCLUDES . FILENAME_FOOTER; ?>
<!-- eof FOOTER -->
