-- phpMyAdmin SQL Dump
-- version 4.4.15.8
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 18, 2016 at 10:34 AM
-- Server version: 10.0.28-MariaDB
-- PHP Version: 5.6.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ingredients`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `Banners_Id` int(11) NOT NULL,
  `Banners_Title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Banners_File` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Banners_Url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Banners_Clicks` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`Banners_Id`, `Banners_Title`, `Banners_File`, `Banners_Url`, `Banners_Clicks`) VALUES
(1, 'W3Schools_local', 'w3schools468x60_simple.gif', 'http://www.w3schools.com/', NULL),
(2, 'W3Schools_remote', 'w3schools468x60.gif', 'http://www.w3schools.com/', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `cat_id` mediumint(11) NOT NULL,
  `cat_first_lang` mediumint(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`cat_id`, `cat_first_lang`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categorie_description`
--

CREATE TABLE IF NOT EXISTS `categorie_description` (
  `cat_id` mediumint(8) NOT NULL,
  `lang_id` mediumint(8) NOT NULL,
  `cat_desc` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categorie_description`
--

INSERT INTO `categorie_description` (`cat_id`, `lang_id`, `cat_desc`) VALUES
(1, 1, 'Meat'),
(2, 1, 'Fish'),
(3, 1, 'Poulty'),
(4, 1, 'Eggs'),
(5, 1, 'Luncheon meat'),
(6, 1, 'Dairy'),
(7, 1, 'Fat''s and oils'),
(8, 1, 'Vagetables'),
(9, 1, 'Beans and legumes'),
(10, 1, 'Nuts and seeds'),
(11, 1, 'Condiments, soices seasonings'),
(12, 1, 'Fruits'),
(13, 1, 'Beverages'),
(14, 1, 'Grains and strachy carbohydrat'),
(15, 1, 'Sweeteners'),
(16, 1, 'Miscellaneous'),
(9, 2, 'Bonen en peulvruchten'),
(13, 2, 'Dranken'),
(6, 2, 'Zuivel'),
(4, 2, 'Eieren'),
(7, 2, 'Vet en oliÃ«n'),
(2, 2, 'Vis'),
(12, 2, 'Fruit'),
(1, 2, 'Vlees'),
(16, 2, 'Overige'),
(10, 2, 'Noten en zaden'),
(3, 2, 'Gevogelte '),
(15, 2, 'Zoetmiddelen'),
(8, 2, 'Groente');

-- --------------------------------------------------------

--
-- Table structure for table `ingredients`
--

CREATE TABLE IF NOT EXISTS `ingredients` (
  `ing_id` mediumint(11) NOT NULL,
  `cat_id` mediumint(11) NOT NULL,
  `ing_first_lang` mediumint(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=186 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ingredients`
--

INSERT INTO `ingredients` (`ing_id`, `cat_id`, `ing_first_lang`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 1),
(4, 1, 1),
(5, 1, 1),
(6, 1, 1),
(7, 1, 1),
(8, 1, 1),
(9, 1, 1),
(10, 1, 1),
(11, 1, 1),
(12, 1, 1),
(13, 4, 1),
(14, 4, 1),
(15, 4, 1),
(16, 5, 1),
(17, 5, 1),
(18, 5, 1),
(19, 5, 1),
(20, 2, 1),
(21, 2, 1),
(22, 2, 1),
(23, 2, 1),
(24, 2, 1),
(25, 2, 1),
(26, 2, 1),
(27, 2, 1),
(28, 2, 1),
(29, 2, 1),
(30, 2, 1),
(31, 2, 1),
(32, 2, 1),
(33, 2, 1),
(34, 2, 1),
(35, 2, 1),
(36, 2, 1),
(37, 2, 1),
(38, 2, 1),
(39, 2, 1),
(40, 2, 1),
(41, 2, 1),
(42, 2, 1),
(43, 2, 1),
(44, 3, 1),
(45, 3, 1),
(46, 3, 1),
(47, 3, 1),
(48, 3, 1),
(49, 4, 1),
(50, 4, 1),
(51, 6, 1),
(52, 6, 1),
(53, 6, 1),
(54, 6, 1),
(55, 6, 1),
(56, 7, 1),
(57, 7, 1),
(58, 7, 1),
(59, 7, 1),
(60, 7, 1),
(61, 7, 1),
(62, 7, 1),
(63, 7, 1),
(64, 7, 1),
(65, 7, 1),
(66, 7, 1),
(67, 7, 1),
(68, 7, 1),
(69, 7, 1),
(70, 7, 1),
(71, 7, 1),
(72, 8, 1),
(73, 8, 1),
(74, 8, 1),
(75, 8, 1),
(76, 8, 1),
(77, 8, 1),
(78, 8, 1),
(79, 8, 1),
(80, 8, 1),
(81, 8, 1),
(82, 8, 1),
(83, 8, 1),
(84, 8, 1),
(85, 8, 1),
(86, 8, 1),
(87, 8, 1),
(88, 8, 1),
(89, 8, 1),
(90, 8, 1),
(91, 8, 1),
(92, 8, 1),
(93, 8, 1),
(94, 8, 1),
(95, 8, 1),
(96, 8, 1),
(97, 9, 1),
(98, 9, 1),
(99, 9, 1),
(100, 9, 1),
(101, 9, 1),
(102, 9, 1),
(103, 9, 1),
(104, 9, 1),
(105, 9, 1),
(106, 9, 1),
(107, 9, 1),
(108, 9, 1),
(109, 9, 1),
(110, 9, 1),
(111, 10, 1),
(112, 10, 1),
(113, 10, 1),
(114, 10, 1),
(115, 10, 1),
(116, 10, 1),
(117, 10, 1),
(118, 10, 1),
(119, 10, 1),
(120, 10, 1),
(121, 10, 1),
(122, 10, 1),
(123, 10, 1),
(124, 10, 1),
(125, 10, 1),
(126, 10, 1),
(127, 10, 1),
(128, 10, 1),
(129, 10, 1),
(130, 10, 1),
(131, 10, 1),
(132, 11, 1),
(133, 11, 1),
(134, 11, 1),
(135, 11, 1),
(136, 11, 1),
(137, 11, 1),
(138, 11, 1),
(139, 11, 1),
(140, 12, 1),
(141, 12, 1),
(142, 12, 1),
(143, 12, 1),
(144, 12, 1),
(145, 12, 1),
(146, 12, 1),
(147, 12, 1),
(148, 12, 1),
(149, 12, 1),
(150, 12, 1),
(151, 12, 1),
(152, 12, 1),
(153, 12, 1),
(154, 12, 1),
(155, 12, 1),
(156, 12, 1),
(157, 12, 1),
(158, 12, 1),
(159, 12, 1),
(160, 12, 1),
(161, 12, 1),
(162, 12, 1),
(163, 12, 1),
(164, 12, 1),
(165, 13, 1),
(166, 13, 1),
(167, 13, 1),
(168, 14, 1),
(169, 14, 1),
(170, 14, 1),
(171, 14, 1),
(172, 14, 1),
(173, 14, 1),
(174, 14, 1),
(175, 14, 1),
(176, 14, 1),
(177, 14, 1),
(178, 14, 1),
(179, 14, 1),
(180, 14, 1),
(181, 15, 1),
(182, 15, 1),
(183, 15, 1),
(184, 15, 1),
(185, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ingredients_description`
--

CREATE TABLE IF NOT EXISTS `ingredients_description` (
  `ing_id` mediumint(11) NOT NULL,
  `lang_id` mediumint(11) NOT NULL,
  `ing_Desc` varchar(35) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ingredients_description`
--

INSERT INTO `ingredients_description` (`ing_id`, `lang_id`, `ing_Desc`) VALUES
(1, 1, 'Beef'),
(2, 1, 'Bacon'),
(3, 1, 'Buffalo'),
(4, 1, 'Elk'),
(5, 1, 'Emu'),
(6, 1, 'Goat'),
(7, 1, 'Ham'),
(8, 1, 'Lamb'),
(9, 1, 'Ostrich'),
(10, 1, 'Pork'),
(11, 1, 'Veal'),
(12, 1, 'venison'),
(2, 2, 'Bacon'),
(1, 2, 'Rundvlees'),
(3, 2, 'Buffel'),
(4, 2, 'Elanden'),
(5, 2, 'Emoe'),
(6, 2, 'Geit'),
(7, 2, 'Ham'),
(8, 2, 'Lam'),
(9, 2, 'Ostrich'),
(10, 2, 'Varken'),
(11, 2, 'Kalfsvlees'),
(12, 2, 'hertevlees'),
(13, 1, 'Chicken eggs'),
(14, 1, 'Duck eggs'),
(15, 1, 'Imitation eggs'),
(13, 2, 'Kippen eieren'),
(14, 2, 'Eenden eieren'),
(16, 1, 'Corned beef'),
(17, 1, 'Ham'),
(18, 1, 'Roast beef'),
(19, 1, 'Turkey'),
(16, 2, 'Corned beef'),
(20, 1, 'Catfish'),
(21, 1, 'Cod'),
(22, 1, 'Eel'),
(23, 1, 'Grouper'),
(24, 1, 'Haddock'),
(25, 1, 'Halibot'),
(26, 1, 'Herring'),
(27, 1, 'Mackerel'),
(28, 1, 'Mahi mahi'),
(29, 1, 'Orange roughy'),
(30, 1, 'Pompano'),
(31, 1, 'Salmon'),
(32, 1, 'Sardines'),
(33, 1, 'Scrod'),
(34, 1, 'Sea bass'),
(35, 1, 'Shark'),
(36, 1, 'Snapper'),
(37, 1, 'Sole'),
(38, 1, 'Squid'),
(39, 1, 'Tilapia'),
(40, 1, 'Trout'),
(41, 1, 'Tuna'),
(42, 1, 'Wahoo'),
(43, 1, 'Whitefish'),
(20, 2, 'Katvis'),
(21, 2, 'Kabeljauw'),
(22, 2, 'Palingachtigen'),
(23, 2, 'Tandbaars '),
(24, 2, 'Schelvissen'),
(25, 2, 'Heilbot'),
(26, 2, 'Haringen'),
(27, 2, 'Makreel'),
(28, 2, 'Mahi mahi'),
(29, 2, 'Keizerbaars'),
(30, 2, 'Pompano'),
(31, 2, 'Zalm'),
(32, 2, 'Sardinen'),
(33, 2, 'Kabeljauw'),
(35, 2, 'Haai'),
(36, 2, '(rode) Snapper'),
(37, 2, 'Zool'),
(38, 2, 'Pijlinktvissen'),
(39, 2, 'Tilapia'),
(40, 2, 'Forel'),
(41, 2, 'Tonijn'),
(42, 2, 'Wahoo'),
(43, 2, 'Witte vis'),
(44, 1, 'Chicken'),
(45, 1, 'Guinea fowl'),
(46, 1, 'Duck'),
(47, 1, 'Cornish game hen'),
(48, 1, 'Turkey'),
(49, 1, 'Fish roe'),
(50, 1, 'Caviar'),
(51, 1, 'Almond milk'),
(53, 1, 'Goat&#039;s milk'),
(54, 1, 'Rice milk'),
(55, 1, 'Soy milk'),
(56, 1, 'Avocado oil'),
(57, 1, 'Canola oil'),
(58, 1, 'Corn oil'),
(59, 1, 'Cottonseed oil'),
(60, 1, 'Peanut oil'),
(61, 1, 'Sesame oil'),
(62, 1, 'Coconut oil'),
(63, 1, 'Olive oil'),
(64, 1, 'Flaxseed oil'),
(65, 1, 'Hempseed oil'),
(66, 1, 'Lard'),
(67, 1, 'Margarine'),
(68, 1, 'Safflower oil'),
(69, 1, 'Shortening'),
(70, 1, 'Soy oil'),
(71, 1, 'Sunflowe oil'),
(72, 1, 'Broccoli'),
(73, 1, 'Asparagus'),
(74, 1, 'Cauliflower'),
(75, 1, 'Cabbage'),
(76, 1, 'Squash'),
(77, 1, 'Beets'),
(78, 1, 'Sprouts'),
(79, 1, 'Carrots'),
(80, 1, 'Celery'),
(81, 1, 'Eggplant'),
(82, 1, 'Garlic'),
(83, 1, 'Orka'),
(84, 1, 'Spinach'),
(85, 1, 'Peas'),
(86, 1, 'String beans'),
(87, 1, 'Artichoke'),
(88, 1, 'Cucumber'),
(89, 1, 'Pumpkinseed'),
(90, 1, 'Onion'),
(91, 1, 'Mushrooms'),
(92, 1, 'Peppers'),
(93, 1, 'Tomatoes'),
(94, 1, 'Corn'),
(95, 1, 'Potato'),
(96, 1, 'Yams'),
(97, 1, 'Lentils'),
(98, 1, 'Soy beans'),
(99, 1, 'Black beans'),
(100, 1, 'Navy beans'),
(101, 1, 'Garbanzo beans'),
(102, 1, 'Tofu'),
(103, 1, 'Kidney beans'),
(104, 1, 'White beans'),
(105, 1, 'Lima beans'),
(106, 1, 'Tempeh'),
(107, 1, 'Pinto beans'),
(108, 1, 'Black-eyed peas'),
(109, 1, 'Red beans'),
(110, 1, 'Broad beans'),
(111, 1, 'Almond butter'),
(112, 1, 'Almonds'),
(113, 1, 'Brazil nuts'),
(114, 1, 'Cashew butter'),
(115, 1, 'Cashews nuts'),
(116, 1, 'Flaxseed'),
(117, 1, 'Haney nuts'),
(118, 1, 'Hazelnuts'),
(119, 1, 'Hempseed'),
(120, 1, 'Hempseed butter'),
(121, 1, 'Macadamia nuts'),
(122, 1, 'Macadamia nuts'),
(123, 1, 'Peanut butter'),
(124, 1, 'Peanuts'),
(125, 1, 'Pecans'),
(126, 1, 'Pumpkinseed butter'),
(127, 1, 'Pumpkinseeds'),
(128, 1, 'Sunflower butter'),
(129, 1, 'Sunflower seeds'),
(130, 1, 'Tahini'),
(131, 1, 'Walnuts'),
(132, 1, 'Salsa'),
(133, 1, 'Guacamole'),
(134, 1, 'Celtic sea salt'),
(135, 1, 'Umeboshi paste'),
(136, 1, 'Tomato sauce'),
(137, 1, 'Apple cider vinegar'),
(138, 1, 'Mustard'),
(139, 1, 'Mayonnaise'),
(140, 1, 'Banana'),
(141, 1, 'Blueberries'),
(142, 1, 'Blackberry'),
(143, 1, 'Cherrues'),
(144, 1, 'Lemon'),
(145, 1, 'Strawberries'),
(146, 1, 'Raspberries'),
(147, 1, 'Grapefruit'),
(148, 1, 'Lime'),
(149, 1, 'Appels'),
(150, 1, 'Grapes'),
(151, 1, 'Peach'),
(152, 1, 'Pears'),
(153, 1, 'Kiwi'),
(154, 1, 'Pomegrantes'),
(155, 1, 'Gauva'),
(156, 1, 'Apricots'),
(157, 1, 'Melon'),
(158, 1, 'Oranges'),
(159, 1, 'Plums'),
(160, 1, 'Pineapple'),
(161, 1, 'Passion fruit'),
(162, 1, 'Mango'),
(163, 1, 'Papaya'),
(105, 2, 'Lima boon'),
(165, 1, 'Sodas'),
(166, 1, 'Coffee'),
(167, 1, 'Coconut water'),
(168, 1, 'Bread'),
(169, 1, 'Oatmeal'),
(170, 1, 'Pastries'),
(171, 1, 'Rice'),
(172, 1, 'Amaranth'),
(173, 1, 'Barley'),
(174, 1, 'Buckwheat'),
(175, 1, 'Kamut'),
(176, 1, 'Millet'),
(177, 1, 'Oats'),
(178, 1, 'Quinoa'),
(179, 1, 'Spelt'),
(180, 1, 'Sprouted bread'),
(181, 1, 'Honey'),
(182, 1, 'Maple syrup'),
(183, 1, 'Stevia'),
(184, 1, 'Sugar'),
(97, 2, 'Linzen'),
(98, 2, 'Soya bonen'),
(104, 2, 'Witte bonen'),
(166, 2, 'Koffie'),
(138, 2, 'Mosterd'),
(139, 2, 'Mayonaise'),
(134, 2, 'Zee Zout'),
(51, 2, 'Amandelmelk'),
(53, 2, 'Gieten melk'),
(55, 2, 'Soya melk'),
(56, 2, 'Avocado olie'),
(58, 2, 'MaÃ¯s olie'),
(59, 2, 'Katoenzaad olie'),
(60, 2, 'Pinda olie'),
(61, 2, 'Sesam olie'),
(62, 2, 'Kokos olie'),
(63, 2, 'Olijf olie'),
(64, 2, 'Lijnzaad olie'),
(65, 2, 'Hennepzaad olie'),
(66, 2, 'Reuzel'),
(67, 2, 'Margarine'),
(68, 2, 'Saffloer olie'),
(70, 2, 'Soja olie'),
(71, 2, 'Zonnebloemolie'),
(140, 2, 'Banaan'),
(141, 2, 'Blauwe bes'),
(142, 2, 'Braam'),
(144, 2, 'Citroen'),
(145, 2, 'Aardbei'),
(146, 2, 'Framboos'),
(147, 2, 'Grapefruit'),
(148, 2, 'Limoen'),
(150, 2, 'Druiven'),
(151, 2, 'Perzik'),
(152, 2, 'Peren'),
(153, 2, 'Kiwi'),
(154, 2, 'Granaatappel'),
(156, 2, 'Abrikoos'),
(157, 2, 'Meloen'),
(158, 2, 'Sinaasappel'),
(159, 2, 'Pruimen'),
(160, 2, 'Ananas'),
(161, 2, 'Passievruchten'),
(162, 2, 'Mango'),
(163, 2, 'Papaja'),
(99, 2, 'Zwarte boon'),
(185, 1, 'Chickpea'),
(185, 2, 'Kikkererwt'),
(101, 2, 'Kikkererwt'),
(172, 2, 'Amaranth'),
(173, 2, 'Gerst '),
(168, 2, 'Brood'),
(174, 2, 'Boekweit ');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `lang_id` mediumint(8) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`lang_id`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Table structure for table `language_description`
--

CREATE TABLE IF NOT EXISTS `language_description` (
  `lang_id` mediumint(8) NOT NULL,
  `fldTransLang_Id` mediumint(8) NOT NULL,
  `lang_desc` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `lang_code` varchar(4) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language_description`
--

INSERT INTO `language_description` (`lang_id`, `fldTransLang_Id`, `lang_desc`, `lang_code`) VALUES
(1, 2, 'English', 'en'),
(2, 2, 'Nederlands', 'nl'),
(3, 2, 'Duits', 'de'),
(4, 0, ' ', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `tblBannerClicks`
--

CREATE TABLE IF NOT EXISTS `tblBannerClicks` (
  `fldBanners_Id` int(11) NOT NULL,
  `fldDateTime` datetime NOT NULL,
  `fldHttpHost` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldHttpUserAgent` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldPhpSelf` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldRemoteAddr` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldRemoteHost` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`Banners_Id`);

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `categorie_description`
--
ALTER TABLE `categorie_description`
  ADD PRIMARY KEY (`cat_id`,`lang_id`);

--
-- Indexes for table `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`ing_id`);

--
-- Indexes for table `ingredients_description`
--
ALTER TABLE `ingredients_description`
  ADD PRIMARY KEY (`ing_id`,`lang_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `language_description`
--
ALTER TABLE `language_description`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `tblBannerClicks`
--
ALTER TABLE `tblBannerClicks`
  ADD PRIMARY KEY (`fldBanners_Id`,`fldDateTime`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `Banners_Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `cat_id` mediumint(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `ing_id` mediumint(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=186;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `lang_id` mediumint(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `language_description`
--
ALTER TABLE `language_description`
  MODIFY `lang_id` mediumint(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tblBannerClicks`
--
ALTER TABLE `tblBannerClicks`
AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
