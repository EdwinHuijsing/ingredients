<?php
/*

  Id: redirect.php,v 1.0 2009/03/07 16:29

*/
// include server parameters
  require('includes/configure.php');
//  include header_app.php start en check funtions
   require(DIR_WS_INCLUDES . "header_app.php");

// Tijdelijk gebruik voor het vewerijderen en het nieuwe aanmaken van een artibut in een entiteit
  /* if ($_GET[redirect] == 'clicks') {
     // $sql1 = 'ALTER TABLE `banners` DROP `Banners_View`';
      fundb_query("ALTER TABLE `banners` DROP `Banners_Views`");
      fundb_query("ALTER TABLE `banners` ADD `Banners_Clicks` int(6) NOT NULL default '0';");
   }*/

// Als ik dit niet doe kan ik het later niet goed gebruiken
   $_BannerUrl ='';

// Get Url
if (IS_NUMERIC($_GET[redirect]) == true) {
    $pBannerId = funHtmlSpec($_GET[redirect] );
    $sql = "SELECT Banners_Id, Banners_Url FROM banners";
    $Query = fundb_query($sql);
    While ($QFetch = Fundb_Fetch($Query) ) {
        $_BannerID = funHtmlSpec($QFetch[Banners_Id]);
        if ( $pBannerId == $_BannerID) {
            $_BannerUrl = funHtmlSpec($QFetch[Banners_Url]);
        }
    }

//Update clicks
   FUN_Banner_Clicks($pBannerId);

//Redirect to the wright url
   $url = $_BannerUrl;
   echo header ("Location: $url");

/* example:
   Place in a blank PHP page
   Change to the URL you want to redirect to
   $URL="http://www.example.com";
   header ("Location: $URL");*/

} else {
  echo '<body onload="self.close();">';
}
?>
